module github.com/jacekolszak/pixiq

go 1.14

require (
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200420212212-258d9bec320e
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
